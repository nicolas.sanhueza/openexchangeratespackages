<?php

return [
    'url' => env('OPENEXCHANGESRATES_URL'),
    'app_id' => env('OPENEXCHANGESRATES_ACCESS_APP_ID')
];