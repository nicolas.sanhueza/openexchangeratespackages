<?php

namespace OpenExchangeRates\Services;

use Illuminate\Support\Facades\Http;

class OpenExchangeRates
{
    protected string $apiUrl;
    protected string $apiKey;
    protected string $baseCurrency;

    public function __construct(string $apiUrl, string $apiKey)
    {
        $this->apiUrl = $apiUrl;
        $this->apiKey = $apiKey;
    }

    public function latest()
    {
        $servicePath = 'latest.json';
        $response = Http::get($this->apiUrl. $servicePath, [
            'app_id' => $this->apiKey,
        ]);
        $arrayRequest = $response->json();
        return $arrayRequest;
    }
}