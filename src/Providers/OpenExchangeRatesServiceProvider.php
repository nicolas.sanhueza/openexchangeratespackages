<?php

namespace OpenExchangeRates\Providers;

use Illuminate\Support\ServiceProvider;
use OpenExchangeRates\Services\OpenExchangeRates;

class OpenExchangeRatesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/openexchangerates.php', 'services.openexchangerates');

        $this->app->singleton(OpenExchangeRates::class, function ($app) {
            return new OpenExchangeRates(
                apiUrl: config('services.openexchangerates.url'),
                apiKey: config('services.openexchangerates.app_id'),
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}